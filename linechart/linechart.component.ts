import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';


@Component({
  selector: 'app-linechart',
  templateUrl: './linechart.component.html',
  styleUrls: ['./linechart.component.scss']
})
export class LinechartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


  data:any = [{
    name: 'mtn',
    data: [500, 700, 555, 444, 777, 877, 944, 567, 666, 789, 456, 654]
 }
 
,{
   name: 'orange',
   data: [600, 650, 670, 700, 780, 888, 890, 900, 920, 970, 990, 1000],
},

];

 highcharts = Highcharts

 chartOptions = {   
  chart: {
     type: "spline"
  },
  title: {
     text: "Monthly Payment Method"
  },
  xAxis:{
     categories:["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
  },
  yAxis: {          
     title:{
        text:"Rate"
     } 
  },
  credits:{
    enabled: false
  },
  series: this.data
};



}
