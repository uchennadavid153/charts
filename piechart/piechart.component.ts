import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts';

let newColor = localStorage.getItem('theme-color')
let displayColor : string;

function showcolor(){
   if(newColor === 'theme-orange'){
      displayColor = '#FF9800'
   }else{
      displayColor = '#111'
   }
   return displayColor 
}

console.log(newColor);

@Component({
  selector: 'app-piechart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.scss']
})
export class PiechartComponent implements OnInit, OnChanges {
   color: any = localStorage.getItem('theme-color')
   changeColor: string = ''
 
   

  constructor() { 
   showcolor()
     // #FF9800
     
  }
   ngOnChanges(): void {
      showcolor()
   }
 
  
  ngOnInit(): void {
   console.log(this.color)
   // this.toggleEvent(this.color)
   
}


  highcharts = Highcharts;
  chartOptions:any = { 
     
     chart : {
      
        type:'pie',
        options3d: {
           enabled: true,
           alpha: 45,
           beta: 0
        }
     },
     title : {
        text: 'Monthly Transaction Stauts'   
     },
     tooltip : {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
     },
    
     plotOptions : {
        pie: {
           allowPointSelect: true,
           cursor: 'pointer',
           depth: 35,
           dataLabels: {
              enabled: true,
              format: '<b>{point.name}%</b>: {point.percentage:.1f} %',
              style: {
                 color: 
                 '#111',
                 background: '#111'
                 
                 
              },
              
           }
        }
     },
     credits:{
      enabled: false
    },
     series : [{
        type: 'pie',
        name: 'User',
        data: [
           ['Jeff',   2.0],
           ['Jude',       6.8],
           {
              name: 'Ekema',
              y: 12.8,
              sliced: true,
              selected: true
           },
           ['Helio',    8.5],
           ['Gerald',     6.2],
           ['Kinsley',      45.5],
           ['Lopex',      2.0],
           ['Akono',      3.8],
           ['George',      1.3],
           ['Nissi',      0.9],
           ['Irish',      4],
        ]
     }]
  };
}


