import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-areachat',
  templateUrl: './areachat.component.html',
  styleUrls: ['./areachat.component.scss']
})
export class AreachatComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


highcharts = Highcharts;
chartOptions:any = {   
   chart: {
     type: 'areaspline'
   },
   title: {
     text: 'Weekly Transactions '
   },
   subtitle : {
      style: {
         position: 'absolute',
         right: '0px',
         bottom: '10px'
      }
   },
   legend : {
      layout: 'vertical',
      align: 'left',
      verticalAlign: 'top',
      x: -150,
      y: 100,
      floating: true,
      borderWidth: 1,
     
      
   },
   xAxis:{
      categories: ['Monday','Tuesday','Wednesday','Thursday',
         'Friday','Saturday','Sunday'] 
   },
   yAxis : {
      title: {
         text: 'Number of units'
      }
   },
   tooltip : {
      shared: true, valueSuffix: ' %'
   },
   plotOptions : {
      area: {
         fillOpacity: 0.5 
      }
   },
   credits:{
     enabled: false
   },
   series: [
      {
         name: 'John',
         data: [1, 3, 3, 10, 12, 15, 20]
      }, 
      
   ]
};


}
